# Compose Challenges

#### Challenge 1
- Create a horizontally scrolling list where each item's image is larger than the card that contains it

<p align="center">
  <img src="https://gitlab.com/kevin.dhvo/code-challenge-a/-/raw/main/design/challenge%201.png" alt="1" width="500"/>
</p>


---

#### Challenge 2

- Create a custom bottom navigation with a cutout in the centre
- Create an expanding menu

<p align="center">
  <img src="https://gitlab.com/kevin.dhvo/code-challenge-a/-/raw/main/design/challenge%202.png" alt="2" width="500"/>
</p>
