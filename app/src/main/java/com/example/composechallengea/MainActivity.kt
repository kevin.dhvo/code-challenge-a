package com.example.composechallengea

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.WindowInsetsController
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Bottom
import androidx.compose.ui.Alignment.Companion.BottomCenter
import androidx.compose.ui.Alignment.Companion.BottomStart
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.Start
import androidx.compose.ui.Alignment.Companion.TopCenter
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalInspectionMode
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composechallengea.ui.components.KevinsCard
import com.example.composechallengea.ui.components.KevinsNav
import com.example.composechallengea.ui.components.KevinsRow
import com.example.composechallengea.ui.theme.ComposeChallengeATheme
import com.google.accompanist.systemuicontroller.SystemUiController
import com.google.accompanist.systemuicontroller.rememberSystemUiController

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeChallengeATheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    KevinsContent()
                }
            }
        }
    }
}

@Composable
fun KevinsContent() {

    // this hides the bottom nav
    val isPreview = LocalInspectionMode.current
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && !isPreview) {

        val systemUiController: SystemUiController = rememberSystemUiController()
        systemUiController.isNavigationBarVisible = false

        (LocalContext.current as Activity).window.insetsController?.let {
            it.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }

    Box(modifier = Modifier
        .fillMaxSize()
        .background(Color.LightGray)
    ) {
        KevinsNav(modifier = Modifier) {
            KevinsRow(modifier = Modifier
                .align(BottomStart)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeChallengeATheme {
        KevinsContent()
    }
}