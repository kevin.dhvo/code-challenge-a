package com.example.composechallengea.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Blue = Color(0xFF2176FF)
val Red = Color(0xFFEC3254)
val Tint = Color(0x80000000)