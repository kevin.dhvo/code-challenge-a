package com.example.composechallengea.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composechallengea.R
import com.example.composechallengea.ui.theme.ComposeChallengeATheme

@Composable
fun KevinsCard(
    title: String,
    desc: String,
    imageResId: Int
) {
    Box(
        modifier = Modifier
            .width(300.dp),
    ) {
        // Background
        Box(
            modifier = Modifier
                .height(90.dp)
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .background(Color.White, RoundedCornerShape(15.dp))
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth(0.5f)
                    .align(Alignment.CenterEnd)
            ) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.End)
                        .padding(start = 10.dp, end = 10.dp, top = 10.dp),
                    text = title,
                    textAlign = TextAlign.Center,
                    fontSize = 18.sp
                )
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.End)
                        .padding(start = 10.dp, end = 10.dp, top = 5.dp, bottom = 10.dp),
                    text = desc,
                    textAlign = TextAlign.Center,
                    fontSize = 14.sp
                )
            }
        }
        // Image
        Row(modifier = Modifier
            .fillMaxWidth(0.5f)
            .align(Alignment.BottomStart)
        ){
            Image(
                modifier = Modifier.fillMaxWidth(),
                painter = painterResource(id = imageResId),
                contentDescription = null,
                contentScale = ContentScale.FillWidth
            )
        }
    }
}

@Composable
fun KevinsRow(modifier: Modifier) {
    LazyRow(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(20.dp),
        contentPadding = PaddingValues(20.dp)
    ) {
        item {
            KevinsCard(
                title = "Beach",
                desc = "Waves, surfers, sandcastles ...",
                imageResId = R.drawable.beach
            )
        }
        item {
            KevinsCard(
                title = "Burger",
                desc = "Buns, lettuce, tomatoes ...",
                imageResId = R.drawable.burger
            )
        }
        item {
            KevinsCard(
                title = "Businesses",
                desc = "Stores, boutiques, home services ...",
                imageResId = R.drawable.place
            )
        }
        item {
            KevinsCard(
                title = "Radio",
                desc = "AM, FM, static ...",
                imageResId = R.drawable.radio
            )
        }
        item {
            KevinsCard(
                title = "Yoga",
                desc = "Lotus, downward dog, tree ...",
                imageResId = R.drawable.yoga
            )
        }
    }
}

@Preview
@Composable
private fun KevinsCardPreview() {
    ComposeChallengeATheme() {
        KevinsCard(
            title = "Burger",
            desc = "Buns, lettuce, tomatoes ...",
            imageResId = R.drawable.burger
        )
    }
}