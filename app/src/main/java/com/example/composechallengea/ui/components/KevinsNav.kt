package com.example.composechallengea.ui.components

import androidx.compose.animation.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import com.example.composechallengea.R
import com.example.composechallengea.ui.theme.Blue
import com.example.composechallengea.ui.theme.ComposeChallengeATheme
import com.example.composechallengea.ui.theme.Red
import com.example.composechallengea.ui.theme.Tint

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun KevinsNav(
    modifier: Modifier,
    content: @Composable BoxScope.() -> Unit
) {

    Box(modifier = Modifier.fillMaxSize()) {

        Box(modifier = Modifier
            .fillMaxSize()
            .padding(bottom = 100.dp)) {
            content()
        }

        var isOpen by remember { mutableStateOf(false) }
        val color by animateColorAsState(targetValue = if (isOpen) Red else Blue )
        val icon = if (isOpen) R.drawable.close else R.drawable.plus

        val density = LocalDensity.current
        val configuration = LocalConfiguration.current

        AnimatedVisibility(
            visible = isOpen,
            enter = slideInVertically(initialOffsetY = { fullHeight -> fullHeight }) + fadeIn(),
            exit = slideOutVertically(targetOffsetY = { fullHeight -> fullHeight }) + fadeOut()
        ) {
            // This Box is neccessary to align content
            Box(modifier = Modifier.fillMaxSize()) {
                // Column Menu Icons
                Column(modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 125.dp)
                ) {
                    KevinsIconColumn()
                }
                Column(modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 125.dp, start = 200.dp)
                ) {
                    KevinsNameColumn()
                }
            }
        }



        // Navbar
        Box(modifier = Modifier
            .fillMaxWidth()
            .align(Alignment.BottomCenter)
            .height(100.dp)) {
            Box(modifier = modifier
                .fillMaxWidth()
                .padding(top = 15.dp)
                .height(100.dp)
                .background(color = Color.White, shape = CutoutShape()),
            ) {

            }

            // Middle Button
            Box(
                modifier = Modifier
                    .size(64.dp)
                    .align(Alignment.TopCenter)
                    .background(color, CircleShape)
                    .clickable { isOpen = !isOpen }
            ) {
                Icon(
                    modifier = Modifier
                        .size(24.dp)
                        .align(Alignment.Center),
                    painter = painterResource(id = icon),
                    contentDescription = null,
                    tint = Color.White
                )
            }
        }
    }
}

@Composable
fun KevinsIconColumn() {
    Column(
        verticalArrangement = Arrangement.spacedBy(20.dp)
    ) {
        KevinsMenuIcon(icon = R.drawable.hospital, name = "Services")
        KevinsMenuIcon(icon = R.drawable.toolbox, name = "Settings")
        KevinsMenuIcon(icon = R.drawable.beach_with_umbrella, name = "Beaches")
        KevinsMenuIcon(icon = R.drawable.hamburger, name = "Fast-food")
        KevinsMenuIcon(icon = R.drawable.fork_and_knife, name = "Restaurant")
    }
}

@Composable
fun KevinsNameColumn() {
    Column(
        verticalArrangement = Arrangement.spacedBy(20.dp)
    ) {
        KevinsMenuName(name = "Services")
        KevinsMenuName(name = "Settings")
        KevinsMenuName(name = "Beaches")
        KevinsMenuName(name = "Fast-food")
        KevinsMenuName(name = "Restaurant")
    }
}


@Composable
fun KevinsMenuIcon(icon: Int, name: String) {
    Box(modifier = Modifier.height(64.dp)) {
        Row(modifier = Modifier,
            verticalAlignment = Alignment.CenterVertically
        ) {
            // Icon
            Box(modifier = Modifier
                .shadow(5.dp, CircleShape)
                .size(64.dp)
                .background(Color.White, CircleShape)
            ) {
                Icon(
                    painter = painterResource(id = icon),
                    contentDescription = null,
                    modifier = Modifier
                        .size(35.dp)
                        .align(Alignment.Center),
                    tint = Color.Unspecified
                )
            }
            // Name
//            Box(modifier = Modifier
//                .padding(start = 20.dp)
//                .background(Tint, RoundedCornerShape(25.dp))
//            ) {
//                Text(
//                    modifier = Modifier.padding(vertical = 5.dp, horizontal = 20.dp),
//                    text = name,
//                    color = Color.White
//                )
//            }
        }
    }
}

@Composable
fun KevinsMenuName(name: String) {
    Box(modifier = Modifier.height(65.dp)) {
        Row(modifier = Modifier.fillMaxHeight(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            // Name
            Box(modifier = Modifier
                .padding(start = 20.dp)
                .background(Tint, RoundedCornerShape(25.dp))
            ) {
                Text(
                    modifier = Modifier.padding(vertical = 5.dp, horizontal = 20.dp),
                    text = name,
                    color = Color.White
                )
            }
        }
    }
}

class CutoutShape() : Shape {

    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline {
        return Outline.Generic(
            path = drawPath(size = size, density = density)
        )
    }
}

fun drawPath(size: Size, density: Density): Path {
    return Path().apply {
        reset()
        lineTo(
            x = with(density) { size.width/2 - 45.dp.toPx() },
            y = 0f
        )
        arcTo(
            rect = Rect(
                left = with(density) { size.width/2 - 90.dp.toPx() },
                top = 0f,
                right = with(density) { size.width/2 - 42.dp.toPx() },
                bottom = with(density) { 45.dp.toPx() }
            ),
            startAngleDegrees = 270f,
            sweepAngleDegrees = 90f,
            forceMoveTo = false
        )
        arcTo(
            rect = Rect(
                left = with(density) { size.width/2 - 45.dp.toPx() },
                top = with(density) { -15-50.dp.toPx() },
                right = with(density) { size.width/2 + 45.dp.toPx() },
                bottom = with(density) { 25+50.dp.toPx() }
            ),
            startAngleDegrees = 160f,
            sweepAngleDegrees = -140f,
            forceMoveTo = false
        )
        arcTo(
            rect = Rect(
                left = with(density) { size.width/2 + 42.dp.toPx() },
                top = 0f,
                right = with(density) { size.width/2 + 90.dp.toPx() },
                bottom = with(density) { 45.dp.toPx() }
            ),
            startAngleDegrees = 180f,
            sweepAngleDegrees = 90f,
            forceMoveTo = false
        )
        lineTo(size.width, 0f)
        lineTo(size.width, size.height)
        lineTo(x = 0f, y = size.height)
        close()
    }
}


@Preview
@Composable
private fun KevinsNavPreview() {
    ComposeChallengeATheme {
        KevinsNav(modifier = Modifier, {})
    }
}

@Preview
@Composable
private fun KevinsMenuIconPreview() {
    ComposeChallengeATheme {
        KevinsMenuIcon(icon = R.drawable.hospital, name = "Services")
    }
}
